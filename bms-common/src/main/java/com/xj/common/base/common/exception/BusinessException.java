package com.xj.common.base.common.exception;

/**
 * @author xj
 *
 */
public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private EnumSvrResult result;
	
	private Message localMessage;
	
	private Messages localMessages = new Messages();

	public BusinessException(String messageCode) {
		super(messageCode);
		Message message = new Message();
		message.setMessageCode(messageCode);
		this.addMessage(message);
	}
	
	public BusinessException(EnumSvrResult result) {
		this.result = result;
	}

	public BusinessException(String messageCode, String message, String...params) {
		super(messageCode);
		Message m = new Message();
		m.setMessageCode(messageCode);
		m.setMessage(message);
		m.setParams(params);
		this.addMessage(m);
	}
	
	public BusinessException(Message message) {
		this.addMessage(message);
	}
	
	public void addMessage(Message m) {
		this.localMessage = m;
		this.localMessages.addMessage(m);
	}

	/**
	 * @return the localMessage
	 */
	public Message getLocalMessage() {
		return localMessage;
	}

	/**
	 * @param localMessage the localMessage to set
	 */
	public void setLocalMessage(Message localMessage) {
		this.localMessage = localMessage;
	}

	/**
	 * @return the localMessages
	 */
	public Messages getLocalMessages() {
		return localMessages;
	}

	/**
	 * @param localMessages the localMessages to set
	 */
	public void setLocalMessages(Messages localMessages) {
		this.localMessages = localMessages;
	}

	public EnumSvrResult getResult() {
		return result;
	}

	public void setResult(EnumSvrResult result) {
		this.result = result;
	}
	
}

