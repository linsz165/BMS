package com.xj.common.base.common.exception;

import java.util.ArrayList;
import java.util.List;


/**
 * @author xj
 *
 */
public class Messages {

	/** */
	private static final long serialVersionUID = 1L;

	public Messages() {
		super();
	}

	private List<Message> message;

	public void addMessage(Message message) {
		if(this.message == null) {
			this.message = new ArrayList<Message>();
		}
		this.message.add(message);
	}
	
	public List<Message> getMessage() {
		return message;
	}

	public void setMessage(List<Message> messages) {
		this.message = messages;
	}
}

