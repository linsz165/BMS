package com.xj.api.config.wechat;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties(prefix = "wechat.mp")
public class WechatMpAccountConfig {
	
	private String appId;
	
	private String appSecret;
	
}
